public class Tank {
    private Float tankVolume;
    public boolean isEmpty;

    //Constructor
    public Tank(Float volume) {
        setTankVolume(volume);
        isEmpty=false;
    }

    //Getters and Setters
    public Float getTankVolume() {
        return tankVolume;
    }
    public void setTankVolume(Float tankVolume) {
        this.tankVolume = tankVolume;
    }

    public void decreaseTankValue(Float value) {
        if ( (getTankVolume() - value) >0 ) {
            Float updatedVolume = getTankVolume() - value;
            setTankVolume(updatedVolume);
        } else {
            System.out.println("Tank is EMPTY!");
            isEmpty=true;
        }
    }

    @Override
    public String toString() {
        return "Tank{" + "tankVolume=" + tankVolume + '}';
    }
}
