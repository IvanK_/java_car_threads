public class Pump extends Thread {

    public Tank tank;
    public Engine engine;
    public Boolean pumpIsStopped;
    public Float internalVolume;

    Pump (Tank tank, Engine engine) {
        this.tank = tank;
        this.engine = engine;
        pumpIsStopped = false;
        internalVolume=0.0f;
    }

    public void takeFromTank () {
        System.out.println("Current volume of fuel in Tank is: " + tank.getTankVolume());
        System.out.println("Current volume of fuel in Pump is: " + internalVolume);
        if (internalVolume <= 0.15f) {
            if (tank.isEmpty) {
                System.out.println("Pump can't take fuel from tank. Tank is empty.");
                pumpIsStopped = true;
            } else {
                System.out.println("Pump is taking " + 0.2f + " litrs from the Tank");
                tank.decreaseTankValue(0.2f);
                internalVolume += 0.2f;
            }
        }
    }
    public void putInEngine() {
            if (internalVolume >= 0.15f) {
                System.out.println("Pump is putting " + 0.15f + " litrs into the Engine");
                internalVolume-=0.15f;
                engine.pourFuelInEngine(0.15f);
            } else {
                System.out.println("Not suffiecent volume of fuel inside the pump - " + internalVolume);
            }
    }

    public void run() {
        Boolean flag = engine.needFuel;
        while (flag && !pumpIsStopped) {
            takeFromTank();
            putInEngine();
            engine.checkNeedFuel();
            flag = engine.needFuel;
        }
        System.out.println("Pump is stopped.");
    }
}
