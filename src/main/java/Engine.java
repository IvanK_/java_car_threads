import java.util.Scanner;

public class Engine extends Thread {
    private Float engineVolume;
    public static final Float oneTackConsumtionVolume = 0.05f;
    public boolean needFuel;

    //Constructor
    Engine (Float volume) {
        setEngineVolume(volume);
        this.needFuel = false;
    }

    //Getters and Setters
    public Float getEngineVolume() {
        return engineVolume;
    }
    public void setEngineVolume(Float volume) {
        engineVolume = volume;
    }

    public void pourFuelInEngine(Float volume) {
        setEngineVolume(getEngineVolume() + volume);
        checkNeedFuel();
    }

    //need fuel
    public void checkNeedFuel() {
        if (getEngineVolume() < 1) needFuel= true;
        else needFuel = false;
    }

    //burning fuel in engine
    public void burnEngineVolume() {
        setEngineVolume(getEngineVolume()- oneTackConsumtionVolume);
        checkNeedFuel();
    }

    public void run() {
        while (!needFuel) {
            System.out.println("Engine is burning " + oneTackConsumtionVolume + " litrs");
            burnEngineVolume();
        }
    }
}
