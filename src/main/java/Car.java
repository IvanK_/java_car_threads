public class Car extends Thread{

    private Engine engine;
    private Pump pump;
    private Tank tank;
    public boolean isStopped;
    public boolean isEngine;

    Car (Engine engine, Pump pump, Tank tank) {
        setEngine(engine);
        setPump(pump);
        setTank(tank);
        isStopped = false;
        isEngine=false;
    }


    //Getters and setters
    public Engine getEngine() {
        return engine;
    }
    public void setEngine(Engine engine) {
        this.engine = engine;
    }
    public Pump getPump() {
        return pump;
    }
    public void setPump(Pump pump) {
        this.pump = pump;
    }
    public Tank getTank() {
        return tank;
    }
    public void setTank(Tank tank) {
        this.tank = tank;
    }



    public void run() {
        Boolean flag = pump.pumpIsStopped;
        System.out.println("Engine is started");
        while (!flag) {
            isEngine = engine.needFuel;

            while (!isEngine) {
                System.out.println("Engine is working");
                engine.run();
                isEngine = engine.needFuel;
            }
            while (isEngine) {
                System.out.println("Pump is working");
                pump.run();
                isEngine = engine.needFuel;
            }
            flag = pump.pumpIsStopped;
        }
        System.out.println("Car is stopped. No more fuel!");
    }
}
