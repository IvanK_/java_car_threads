public class ExecCar {
    public static void main(String[] args) {
        Tank tank = new Tank (100f);
        Engine engine = new Engine (2f);
        Pump pump = new Pump(tank,engine);
        Car car = new Car (engine, pump, tank);
        car.start();
    }
}
